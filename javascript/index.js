var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var MongoClient = require('mongodb').MongoClient;

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

var gameId = "gameId";
var raceId = "raceId";
var serial = 0;

var log = function(data,server) {
  data._date = Date();
  data._gameId = gameId;
  data._raceId = raceId;
  data._server = server;
  data._serial = serial++;
  logf(data);
}

var logf = function(data) {
   //console.log(JSON.stringify(data));
}

MongoClient.connect('mongodb://127.0.0.1:27017/HWO', function(err, db) {
  if(err) {
    console.log("No database to log");
    return;
  }
  var collection = db.collection('RACE');
  
  logf = function(data)
  {
    collection.insert(data, function(err, docs) {
      if(err) console.log(err);
    });
  }
});

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});


function send(json) {
  client.write(JSON.stringify(json));
  log(json,0);
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  log(data,1);

  if (data.msgType === 'carPositions') {
    send({
      msgType: "throttle",
      data: 0.9
    });
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'crash') {
      console.log('Someone crashed');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (data.msgType === 'yourCar') {
      gameId = data.gameId;
    } else if (data.msgType === 'gameInit') {
      raceId = data.data.race.track.id;
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
